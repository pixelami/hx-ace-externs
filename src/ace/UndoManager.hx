package ace;

@extern('UndoManager')
extern class UndoManager
{
	/**
	Provides a means for implementing your own undo manager. `options` has one property, `args`, an [[Array `Array`]], with two elements:
* `args[0]` is an array of deltas
* `args[1]` is the document to associate with
	@arg options Contains additional properties
	*/
	public function execute(options:Dynamic):Void;
	

	/**
	Returns `true` if there are redo operations left to perform.
	*/
	public function hasRedo():Bool;
	

	/**
	Returns `true` if there are undo operations left to perform.
	*/
	public function hasUndo():Bool;
	

	/**
	[Perform a redo operation on the document, reimplementing the last change.]{: #UndoManager.redo}
	@arg dontSelect {:dontSelect}
	*/
	public function redo(dontSelect:Bool):Void;
	

	/**
	Destroys the stack of undo and redo redo operations.
	*/
	public function reset():Void;
	

	/**
	[Perform an undo operation on the document, reverting the last change. Returns the range of the undo.]{: #UndoManager.undo}
	@arg dontSelect {:dontSelect}
	*/
	public function undo(dontSelect:Bool):Range;
	


}
