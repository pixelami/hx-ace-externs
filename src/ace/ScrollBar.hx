package ace;

@extern('ScrollBar')
extern class ScrollBar
{
	/**
	Returns the width of the scroll bar.
	*/
	public function getWidth():Float;
	

	/**
	Sets the height of the scroll bar, in pixels.
	@arg height The new height
	*/
	public function setHeight(height:Float):Void;
	

	/**
	Sets the inner height of the scroll bar, in pixels.
	@arg height The new inner height
	*/
	public function setInnerHeight(height:Float):Void;
	

	/**
	Sets the scroll top of the scroll bar.
	@arg scrollTop The new scroll top
	*/
	public function setScrollTop(scrollTop:Float):Void;
	


}
