package ace;
import js.Dom;

@extern('ace')
extern interface Ace
{
	/**
	This method embeds the Ace editor into the DOM, at the element provided by `el`.
	@arg el Either the id of an element, or the element itself
	*/

	@:overload(function(el:js.HtmlDom):Editor{})
	public function edit(el:String):Editor;
}
