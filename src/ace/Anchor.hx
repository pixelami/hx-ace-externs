package ace;

@extern('Anchor')
extern class Anchor
{
	/**
	Clips the anchor position to the specified row and column.
	@arg row The row index to clip the anchor to
	@arg column The column index to clip the anchor to
	*/
	public function clipPositionToDocument(row:Float, column:Float):Void;
	

	/**
	When called, the `'change'` event listener is removed.
	*/
	public function detach():Void;
	

	/**
	Returns the current document.
	*/
	public function getDocument():Document;
	

	/**
	Returns an object identifying the `row` and `column` position of the current anchor.
	*/
	public function getPosition():Dynamic;
	

	/**
	Sets the anchor position to the specified row and column. If `noClip` is `true`, the position is not clipped.
	@arg row The row index to move the anchor to
	@arg column The column index to move the anchor to
	@arg noClip Identifies if you want the position to be clipped
	*/
	public function setPosition(row:Float, column:Float, noClip:Bool):Void;
	


}
