package ace;

import js.Dom;
@extern('VirtualRenderer')
extern class VirtualRenderer
{
	/**
	Deprecated (moved to EditSession)
	*/
	public function addGutterDecoration(row:Dynamic, className:Dynamic):Void;
	

	/**
	Adjusts the wrap limit, which is the number of characters that can fit within the width of the edit area on screen.
	*/
	public function adjustWrapLimit():Void;
	

	/**
	Destroys the text and cursor layers for this renderer.
	*/
	public function destroy():Void;
	

	/**
	Returns whether an animated scroll happens or not.
	*/
	public function getAnimatedScroll():Bool;
	

	/**
	Returns the root element containing this renderer.
	*/
	public function getContainerElement():HtmlDom;
	

	/**
	Returns the index of the first fully visible row. "Fully" here means that the characters in the row are not truncated; that the top and the bottom of the row are on the screen.
	*/
	public function getFirstFullyVisibleRow():Float;
	

	/**
	[Returns the index of the first visible row.]{: #VirtualRenderer.getFirstVisibleRow}
	*/
	public function getFirstVisibleRow():Float;
	

	/**
	Returns whether the horizontal scrollbar is set to be always visible.
	*/
	public function getHScrollBarAlwaysVisible():Bool;
	

	/**
	Returns the index of the last fully visible row. "Fully" here means that the characters in the row are not truncated; that the top and the bottom of the row are on the screen.
	*/
	public function getLastFullyVisibleRow():Float;
	

	/**
	[Returns the index of the last visible row.]{: #VirtualRenderer.getLastVisibleRow}
	*/
	public function getLastVisibleRow():Float;
	

	/**
	Returns the element that the mouse events are attached to
	*/
	public function getMouseEventTarget():HtmlDom;
	

	/**
	Returns whether the print margin column is being shown or not.
	*/
	public function getPrintMarginColumn():Bool;
	

	/**
	Returns the last visible row, regardless of whether it's fully visible or not.
	*/
	public function getScrollBottomRow():Float;
	

	/**
	{:EditSession.getScrollLeft}
	*/
	public function getScrollLeft():Float;
	

	/**
	{:EditSession.getScrollTop}
	*/
	public function getScrollTop():Float;
	

	/**
	Returns the first visible row, regardless of whether it's fully visible or not.
	*/
	public function getScrollTopRow():Float;
	

	/**
	Returns `true` if the gutter is being shown.
	*/
	public function getShowGutter():Bool;
	

	/**
	Returns whether invisible characters are being shown or not.
	*/
	public function getShowInvisibles():Bool;
	

	/**
	Returns whetherthe print margin is being shown or not.
	*/
	public function getShowPrintMargin():Bool;
	

	/**
	Returns the element to which the hidden text area is added.
	*/
	public function getTextAreaContainer():HtmlDom;
	

	/**
	[Returns the path of the current theme.]{: #VirtualRenderer.getTheme}
	*/
	public function getTheme():String;
	

	/**
	Hides the current composition.
	*/
	public function hideComposition():Void;
	

	/**
	Hides the cursor icon.
	*/
	public function hideCursor():Void;
	

	/**
	Returns `true` if you can still scroll by either parameter; in other words, you haven't reached the end of the file or line.
	@arg deltaX The x value to scroll by
	@arg deltaY The y value to scroll by
	*/
	public function isScrollableBy(deltaX:Float, deltaY:Float):Bool;
	

	/**
	[Triggers a resize of the editor.]{: #VirtualRenderer.onResize}
	@arg force If `true`, recomputes the size, even if the height and width haven't changed
	*/
	public function onResize(force:Bool):Void;
	

	/**
	Deprecated (moved to EditSession)
	*/
	public function removeGutterDecoration(row:Dynamic, className:Dynamic):Void;
	

	/**
	Scrolls the editor across both x- and y-axes.
	@arg deltaX The x value to scroll by
	@arg deltaY The y value to scroll by
	*/
	public function scrollBy(deltaX:Float, deltaY:Float):Void;
	

	/**
	Scrolls the cursor into the first visibile area of the editor
	*/
	public function scrollCursorIntoView(cursor:Dynamic, offset:Dynamic):Void;
	

	/**
	Gracefully scrolls the editor to the row indicated.
	@arg line A line number
	@arg center If `true`, centers the editor the to indicated line
	@arg animate If `true` animates scrolling
	@arg callback Function to be called after the animation has finished
	*/
	public function scrollToLine(line:Float, center:Bool, animate:Bool, _callback:Dynamic):Void;
	

	/**
	Gracefully scrolls the top of the editor to the row indicated.
	@arg row A row id
	*/
	public function scrollToRow(row:Float):Void;
	

	/**
	Scrolls the editor to the x pixel indicated.
	@arg scrollLeft The position to scroll to
	*/
	public function scrollToX(scrollLeft:Float):Float;
	

	/**
	Scrolls the editor to the y pixel indicated.
	@arg scrollTop The position to scroll to
	*/
	public function scrollToY(scrollTop:Float):Float;
	

	/**
	Identifies whether you want to have an animated scroll or not.
	@arg shouldAnimate Set to `true` to show animated scrolls
	*/
	public function setAnimatedScroll(shouldAnimate:Bool):Void;
	

	/**
	Sets annotations for the gutter.
	@arg annotations An array containing annotations
	*/
	public function setAnnotations(annotations:Array<Dynamic>):Void;
	

	/**
	Sets the inner text of the current composition to `text`.
	@arg text A string of text to use
	*/
	public function setCompositionText(text:String):Void;
	

	/**
	Identifies whether you want to show the horizontal scrollbar or not.
	@arg alwaysVisible Set to `true` to make the horizontal scroll bar visible
	*/
	public function setHScrollBarAlwaysVisible(alwaysVisible:Bool):Void;
	

	/**
	Sets the padding for all the layers.
	@arg padding A new padding value (in pixels)
	*/
	public function setPadding(padding:Float):Void;
	

	/**
	Identifies whether you want to show the print margin column or not.
	@arg showPrintMargin Set to `true` to show the print margin column
	*/
	public function setPrintMarginColumn(showPrintMargin:Bool):Void;
	

	/**
	Associates an [[EditSession `EditSession`]].
	*/
	public function setSession(session:Dynamic):Void;
	

	/**
	Identifies whether you want to show the gutter or not.
	@arg show Set to `true` to show the gutter
	*/
	public function setShowGutter(show:Bool):Void;
	

	/**
	Identifies whether you want to show invisible characters or not.
	@arg showInvisibles Set to `true` to show invisibles
	*/
	public function setShowInvisibles(showInvisibles:Bool):Void;
	

	/**
	Identifies whether you want to show the print margin or not.
	@arg showPrintMargin Set to `true` to show the print margin
	*/
	public function setShowPrintMargin(showPrintMargin:Bool):Void;
	

	/**
	[Adds a new class, `style`, to the editor.]{: #VirtualRenderer.setStyle}
	@arg style A class name
	*/
	public function setStyle(style:String):Void;
	

	/**
	[Sets a new theme for the editor. `theme` should exist, and be a directory path, like `ace/theme/textmate`.]{: #VirtualRenderer.setTheme}
	@arg theme The path to a theme
	*/
	public function setTheme(theme:String):Void;
	

	/**
	
	@arg position 
	*/
	public function showComposition(position:Float):Void;
	

	/**
	Shows the cursor icon.
	*/
	public function showCursor():Void;
	

	/**
	Returns an object containing the `pageX` and `pageY` coordinates of the document position.
	@arg row The document row position
	@arg column The document column position
	*/
	public function textToScreenCoordinates(row:Float, column:Float):Dynamic;
	

	/**
	[Removes the class `style` from the editor.]{: #VirtualRenderer.unsetStyle}
	@arg style A class name
	*/
	public function unsetStyle(style:String):Void;
	

	/**
	Schedules an update to all the back markers in the document.
	*/
	public function updateBackMarkers():Void;
	

	/**
	Redraw breakpoints.
	*/
	public function updateBreakpoints():Void;
	

	/**
	Updates the cursor icon.
	*/
	public function updateCursor():Void;
	

	/**
	Updates the font size.
	*/
	public function updateFontSize():Void;
	

	/**
	Schedules an update to all the front markers in the document.
	*/
	public function updateFrontMarkers():Void;
	

	/**
	Triggers a full update of all the layers, for all the rows.
	*/
	public function updateFull():Void;
	

	/**
	Triggers a partial update of the text, from the range given by the two parameters.
	@arg firstRow The first row to update
	@arg lastRow The last row to update
	*/
	public function updateLines(firstRow:Float, lastRow:Float):Void;
	

	/**
	Triggers a full update of the text, for all the rows.
	*/
	public function updateText():Void;
	

	/**
	Blurs the current container.
	*/
	public function visualizeBlur():Void;
	

	/**
	Focuses the current container.
	*/
	public function visualizeFocus():Void;
	


}
