package ace;

@extern('EditSession')
extern class EditSession
{


	@:overload(function(doc:String, mode:TextMode):Void{})
	@:overload(function(doc:String, mode:String):Void{})
	public function new(doc:Document, mode:TextMode):Void;

	/**
	
	*/
	@extern('$computeWrapSplits') public function computeWrapSplits(tokens:Dynamic, wrapLimit:Dynamic):Array<Dynamic>;
	

	/**
	
	*/
	@extern('$constrainWrapLimit') public function constrainWrapLimit(wrapLimit:Dynamic):Void;
	

	/**
	If `text` contains either the newline (`\n`) or carriage-return ('\r') characters, `$autoNewLine` stores that value.
	@arg text A block of text
	*/
	@extern('$detectNewLine') public function detectNewLine(text:String):Void;
	

	/**
	Given a string, returns an array of the display characters, including tabs and spaces.
	@arg str The string to check
	@arg offset The value to start at
	*/
	@extern('$getDisplayTokens') public function getDisplayTokens(str:String, offset:Float):Array<Dynamic>;
	

	/**
	Calculates the width of the string `str` on the screen while assuming that the string starts at the first column on the screen.
	@arg str The string to calculate the screen width of
	@arg maxScreenColumn 
	@arg screenColumn 
	*/
	@extern('$getStringScreenWidth') public function getStringScreenWidth(str:String, maxScreenColumn:Float, screenColumn:Float):Float;
	

	/**
	
	*/
	@extern('$getUndoSelection') public function getUndoSelection(deltas:Dynamic, isUndo:Dynamic, lastUndoRange:Dynamic):Range;
	

	/**
	
	@arg row The row to work with
	*/
	@extern('$resetRowCache') public function resetRowCache(row:Float):Void;
	

	/**
	
	*/
	@extern('$startWorker') public function startWorker():Void;
	

	/**
	
	*/
	@extern('$syncInformUndoManager') public function syncInformUndoManager():Void;
	

	/**
	
	*/
	@extern('$updateInternalDataOnChange') public function updateInternalDataOnChange():Void;
	

	/**
	
	*/
	@extern('$updateWrapData') public function updateWrapData(firstRow:Dynamic, lastRow:Dynamic):Void;
	

	/**
	Adds a dynamic marker to the session.
	@arg marker object with update method
	@arg inFront Set to `true` to establish a front marker
	*/
	public function addDynamicMarker(marker:Dynamic, inFront:Bool):Dynamic;
	

	/**
	Adds `className` to the `row`, to be used for CSS stylings and whatnot.
	@arg row The row number
	@arg className The class to add
	*/
	public function addGutterDecoration(row:Float, className:String):Void;
	

	/**
	Adds a new marker to the given `Range`. If `inFront` is `true`, a front marker is defined, and the `'changeFrontMarker'` event fires; otherwise, the `'changeBackMarker'` event fires.
	@arg range Define the range of the marker
	@arg clazz Set the CSS class for the marker
	@arg type Identify the type of the marker
	@arg inFront Set to `true` to establish a front marker
	*/
	public function addMarker(range:Range, clazz:String, type:Dynamic, inFront:Bool):Float;
	

	/**
	This should generally only be called by the renderer when a resize is detected.
	@arg desiredLimit The new wrap limit
	*/
	public function adjustWrapLimit(desiredLimit:Float):Bool;
	

	/**
	Clears all the annotations for this session. This function also triggers the `'changeAnnotation'` event.
	*/
	public function clearAnnotations():Void;
	

	/**
	Removes a breakpoint on the row number given by `rows`. This function also emites the `'changeBreakpoint'` event.
	@arg row A row index
	*/
	public function clearBreakpoint(row:Float):Void;
	

	/**
	Removes all breakpoints on the rows. This function also emites the `'changeBreakpoint'` event.
	*/
	public function clearBreakpoints():Void;
	

	/**
	
	*/
	public function documentToScreenColumn(row:Dynamic, docColumn:Dynamic):Float;
	

	/**
	Converts document coordinates to screen coordinates. {:conversionConsiderations}
	@arg docRow The document row to check
	@arg docColumn The document column to check
	*/
	public function documentToScreenPosition(docRow:Float, docColumn:Float):Dynamic;
	

	/**
	
	*/
	public function documentToScreenRow(docRow:Dynamic, docColumn:Dynamic):Float;
	

	/**
	Duplicates all the text between `firstRow` and `lastRow`.
	@arg firstRow The starting row to duplicate
	@arg lastRow The final row to duplicate
	*/
	public function duplicateLines(firstRow:Float, lastRow:Float):Float;
	

	/**
	Returns the annotations for the `EditSession`.
	*/
	public function getAnnotations():Dynamic;
	

	/**
	Gets the range of a word, including its right whitespace.
	@arg row The row number to start from
	@arg column The column number to start from
	*/
	public function getAWordRange(row:Float, column:Float):Range;
	

	/**
	Returns an array of numbers, indicating which rows have breakpoints.
	*/
	public function getBreakpoints():Array<Dynamic>;
	

	/**
	Returns the `Document` associated with this session.
	*/
	public function getDocument():Document;
	

	/**
	
	@arg docRow 
	@arg docColumn 
	*/
	public function getDocumentLastRowColumn(docRow:Float, docColumn:Float):Float;
	

	/**
	
	*/
	public function getDocumentLastRowColumnPosition(docRow:Dynamic, docColumn:Dynamic):Float;
	

	/**
	Returns the number of rows in the document.
	*/
	public function getLength():Float;
	

	/**
	Returns a verbatim copy of the given line as it is in the document
	@arg row The row to retrieve from
	*/
	public function getLine(row:Float):String;
	

	/**
	Returns an array of strings of the rows between `firstRow` and `lastRow`. This function is inclusive of `lastRow`.
	@arg firstRow The first row index to retrieve
	@arg lastRow The final row index to retrieve
	*/
	public function getLines(firstRow:Float, lastRow:Float):Array<Dynamic>;
	

	/**
	Returns an array containing the IDs of all the markers, either front or back.
	@arg inFront If `true`, indicates you only want front markers; `false` indicates only back markers
	*/
	public function getMarkers(inFront:Bool):Array<Dynamic>;
	

	/**
	Returns the current text mode.
	*/
	public function getMode():TextMode;
	

	/**
	Returns the current new line mode.
	*/
	public function getNewLineMode():String;
	

	/**
	Returns `true` if overwrites are enabled; `false` otherwise.
	*/
	public function getOverwrite():Bool;
	

	/**
	Returns number of screenrows in a wrapped line.
	@arg row The row number to check
	*/
	public function getRowLength(row:Float):Float;
	

	/**
	
	*/
	public function getRowSplitData(row:Dynamic):Dynamic;
	

	/**
	Returns the column position (on screen) for the last character in the provided row.
	@arg screenRow The screen row to check
	*/
	public function getScreenLastRowColumn(screenRow:Float):Float;
	

	/**
	Returns the length of the screen.
	*/
	public function getScreenLength():Float;
	

	/**
	The distance to the next tab stop at the specified screen column.
	@arg screenColumn The screen column to check
	*/
	public function getScreenTabSize(screenColumn:Float):Float;
	

	/**
	Returns the width of the screen.
	*/
	public function getScreenWidth():Float;
	

	/**
	[Returns the value of the distance between the left of the editor and the leftmost part of the visible content.]{: #EditSession.getScrollLeft}
	*/
	public function getScrollLeft():Float;
	

	/**
	[Returns the value of the distance between the top of the editor and the topmost part of the visible content.]{: #EditSession.getScrollTop}
	*/
	public function getScrollTop():Float;
	

	/**
	Returns the string of the current selection.
	*/
	public function getSelection():Selection;
	

	/**
	{:BackgroundTokenizer.getState}
	@arg row The row to start at
	*/
	public function getState(row:Float):Array<Dynamic>;
	

	/**
	Returns the current tab size.
	*/
	public function getTabSize():Float;
	

	/**
	Returns the current value for tabs. If the user is using soft tabs, this will be a series of spaces (defined by [[EditSession.getTabSize `getTabSize()`]]); otherwise it's simply `'\t'`.
	*/
	public function getTabString():String;
	

	/**
	{:Document.getTextRange.desc}
	@arg range The range to work with
	*/
	public function getTextRange(range:String):Array<Dynamic>;
	

	/**
	Returns an array of tokens at the indicated row and column.
	@arg row The row number to retrieve from
	@arg column The column number to retrieve from
	*/
	public function getTokenAt(row:Float, column:Float):Array<Dynamic>;
	

	/**
	Starts tokenizing at the row indicated. Returns a list of objects of the tokenized rows.
	@arg row The row to start at
	*/
	public function getTokens(row:Float):Array<Dynamic>;
	

	/**
	Returns the current undo manager.
	*/
	public function getUndoManager():UndoManager;
	

	/**
	Returns `true` if soft tabs are being used, `false` otherwise.
	*/
	public function getUseSoftTabs():Bool;
	

	/**
	Returns `true` if workers are being used.
	*/
	public function getUseWorker():Bool;
	

	/**
	Returns `true` if wrap mode is being used; `false` otherwise.
	*/
	public function getUseWrapMode():Bool;
	

	/**
	Returns the current [[Document `Document`]] as a string.
	*/
	public function getValue():String;
	

	/**
	Given a starting row and column, this method returns the `Range` of the first word boundary it finds.
	@arg row The row to start at
	@arg column The column to start at
	*/
	public function getWordRange(row:Float, column:Float):Range;
	

	/**
	Returns the value of wrap limit.
	*/
	public function getWrapLimit():Float;
	

	/**
	Returns an object that defines the minimum and maximum of the wrap limit; it looks something like this:

    { min: wrapLimitRange_min, max: wrapLimitRange_max }
	*/
	public function getWrapLimitRange():Dynamic;
	

	/**
	Indents all the rows, from `startRow` to `endRow` (inclusive), by prefixing each row with the token in `indentString`.

If `indentString` contains the `'\t'` character, it's replaced by whatever is defined by [[EditSession.getTabString `getTabString()`]].
	@arg startRow Starting row
	@arg endRow Ending row
	@arg indentString The indent token
	*/
	public function indentRows(startRow:Float, endRow:Float, indentString:String):Void;
	

	/**
	Inserts a block of `text` and the indicated `position`.
	@arg position The position to start inserting at
	@arg text A chunk of text to insert
	*/
	public function insert(position:Float, text:String):Float;
	

	/**
	Returns `true` if the character at the position is a soft tab.
	@arg position The position to check
	*/
	public function isTabStop(position:Dynamic):Bool;
	

	/**
	
	@arg firstRow The starting row to move down
	@arg lastRow The final row to move down
	*/
	public function moveLinesDown(firstRow:Float, lastRow:Float):Float;
	

	/**
	Shifts all the lines in the document up one, starting from `firstRow` and ending at `lastRow`.
	@arg firstRow The starting row to move up
	@arg lastRow The final row to move up
	*/
	public function moveLinesUp(firstRow:Float, lastRow:Float):Float;
	

	/**
	{ row: newRowLocation, column: newColumnLocation }
	@arg fromRange The range of text you want moved within the document
	@arg toPosition The location (row and column) where you want to move the text to
	*/
	public function moveText(fromRange:Range, toPosition:Dynamic):Range;
	

	/**
	Reloads all the tokens on the current session. This function calls [[BackgroundTokenizer.start `BackgroundTokenizer.start ()`]] to all the rows; it also emits the `'tokenizerUpdate'` event.
	*/
	public function onReloadTokenizer(e:Dynamic):Void;
	

	/**
	Outdents all the rows defined by the `start` and `end` properties of `range`.
	@arg range A range of rows
	*/
	public function outdentRows(range:Range):Void;
	

	/**
	Re-implements a previously undone change to your document.
	@arg deltas An array of previous changes
	@arg dontSelect {:dontSelect}
	*/
	public function redoChanges(deltas:Array<Dynamic>, dontSelect:Bool):Range;
	

	/**
	Removes the `range` from the document.
	@arg range A specified Range to remove
	*/
	public function remove(range:Range):Dynamic;
	

	/**
	Removes `className` from the `row`.
	@arg row The row number
	@arg className The class to add
	*/
	public function removeGutterDecoration(row:Float, className:String):Void;
	

	/**
	Removes the marker with the specified ID. If this marker was in front, the `'changeFrontMarker'` event is emitted. If the marker was in the back, the `'changeBackMarker'` event is emitted.
	@arg markerId A number representing a marker
	*/
	public function removeMarker(markerId:Float):Void;
	

	/**
	Replaces a range in the document with the new `text`.
	@arg range A specified Range to replace
	@arg text The new text to use as a replacement
	*/
	public function replace(range:Range, text:String):Dynamic;
	

	/**
	
	*/
	public function screenToDocumentColumn(screenRow:Dynamic, screenColumn:Dynamic):Float;
	

	/**
	Converts characters coordinates on the screen to characters coordinates within the document. [This takes into account code folding, word wrap, tab size, and any other visual modifications.]{: #conversionConsiderations}
	@arg screenRow The screen row to check
	@arg screenColumn The screen column to check
	*/
	public function screenToDocumentPosition(screenRow:Float, screenColumn:Float):Dynamic;
	

	/**
	
	*/
	public function screenToDocumentRow(screenRow:Dynamic, screenColumn:Dynamic):Float;
	

	/**
	Sets annotations for the `EditSession`. This functions emits the `'changeAnnotation'` event.
	@arg annotations A list of annotations
	*/
	public function setAnnotations(annotations:Array<Dynamic>):Void;
	

	/**
	Sets a breakpoint on the row number given by `rows`. This function also emites the `'changeBreakpoint'` event.
	@arg row A row index
	@arg className Class of the breakpoint
	*/
	public function setBreakpoint(row:Float, className:String):Void;
	

	/**
	Sets a breakpoint on every row number given by `rows`. This function also emites the `'changeBreakpoint'` event.
	@arg rows An array of row indicies
	*/
	public function setBreakpoints(rows:Array<Dynamic>):Void;
	

	/**
	Sets the `EditSession` to point to a new `Document`. If a `BackgroundTokenizer` exists, it also points to `doc`.
	@arg doc The new `Document` to use
	*/
	public function setDocument(doc:Document):Void;
	

	/**
	Sets a new text mode for the `EditSession`. This method also emits the `'changeMode'` event. If a [[BackgroundTokenizer `BackgroundTokenizer`]] is set, the `'tokenizerUpdate'` event is also emitted.
	@arg mode Set a new text mode
	*/
	public function setMode(mode:Dynamic /*TextMode*/):Void;
	

	/**
	{:Document.setNewLineMode.desc}
	@arg newLineMode {:Document.setNewLineMode.param}
	*/
	public function setNewLineMode(newLineMode:String):Void;
	

	/**
	Pass in `true` to enable overwrites in your session, or `false` to disable. If overwrites is enabled, any text you enter will type over any text after it. If the value of `overwrite` changes, this function also emites the `changeOverwrite` event.
	@arg overwrite Defines wheter or not to set overwrites
	*/
	public function setOverwrite(overwrite:Bool):Void;
	

	/**
	[Sets the value of the distance between the left of the editor and the leftmost part of the visible content.]{: #EditSession.setScrollLeft}
	*/
	public function setScrollLeft(scrollLeft:Dynamic):Void;
	

	/**
	This function sets the scroll top value. It also emits the `'changeScrollTop'` event.
	@arg scrollTop The new scroll top value
	*/
	public function setScrollTop(scrollTop:Float):Void;
	

	/**
	Set the number of spaces that define a soft tab; for example, passing in `4` transforms the soft tabs to be equivalent to four spaces. This function also emits the `changeTabSize` event.
	@arg tabSize The new tab size
	*/
	public function setTabSize(tabSize:Float):Void;
	

	/**
	Sets the undo manager.
	@arg undoManager The new undo manager
	*/
	public function setUndoManager(undoManager:UndoManager):Void;
	

	/**
	ENables or disables highlighting of the range where an undo occured.
	@arg enable If `true`, selects the range of the reinserted change
	*/
	public function setUndoSelect(enable:Bool):Void;
	

	/**
	Pass `true` to enable the use of soft tabs. Soft tabs means you're using spaces instead of the tab character (`'\t'`).
	@arg useSoftTabs Value indicating whether or not to use soft tabs
	*/
	public function setUseSoftTabs(useSoftTabs:Bool):Void;
	

	/**
	Identifies if you want to use a worker for the `EditSession`.
	@arg useWorker Set to `true` to use a worker
	*/
	public function setUseWorker(useWorker:Bool):Void;
	

	/**
	Sets whether or not line wrapping is enabled. If `useWrapMode` is different than the current value, the `'changeWrapMode'` event is emitted.
	@arg useWrapMode Enable (or disable) wrap mode
	*/
	public function setUseWrapMode(useWrapMode:Bool):Void;
	

	/**
	Sets the session text.
	@arg text The new text to place
	*/
	public function setValue(text:String):Void;
	

	/**
	Sets the boundaries of wrap. Either value can be `null` to have an unconstrained wrap, or, they can be the same number to pin the limit. If the wrap limits for `min` or `max` are different, this method also emits the `'changeWrapMode'` event.
	@arg min The minimum wrap value (the left side wrap)
	@arg max The maximum wrap value (the right side wrap)
	*/
	public function setWrapLimitRange(min:Float, max:Float):Void;
	

	/**
	
	*/
	public function stopWorker():Void;
	

	/**
	Sets the value of overwrite to the opposite of whatever it currently is.
	*/
	public function toggleOverwrite():Void;
	

	/**
	Returns the current [[Document `Document`]] as a string.
	*/
	public function toString():String;
	

	/**
	Reverts previous changes to your document.
	@arg deltas An array of previous changes
	@arg dontSelect [If `true`, doesn't select the range of where the change occured]{: #dontSelect}
	*/
	public function undoChanges(deltas:Array<Dynamic>, dontSelect:Bool):Range;
	


}
