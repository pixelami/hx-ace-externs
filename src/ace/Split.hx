package ace;

@extern('Split')
extern class Split
{
	/**
	Blurs the current editor.
	*/
	public function blur():Void;
	

	/**
	Focuses the current editor.
	*/
	public function focus():Void;
	

	/**
	Executes `callback` on all of the available editors.
	@arg callback A callback function to execute
	@arg scope 
	*/
	public function forEach(_callback:Dynamic, scope:String):Void;
	

	/**
	Returns the current editor.
	*/
	public function getCurrentEditor():Editor;
	

	/**
	Returns the editor identified by the index `idx`.
	@arg idx The index of the editor you want
	*/
	public function getEditor(idx:Float):Editor;
	

	/**
	Returns the orientation.
	*/
	public function getOrientation():Float;
	

	/**
	Returns the number of splits.
	*/
	public function getSplits():Float;
	

	/**
	
	*/
	public function resize():Void;
	

	/**
	Sets the font size, in pixels, for all the available editors.
	@arg size The new font size
	*/
	public function setFontSize(size:Float):Void;
	

	/**
	
	@arg keybinding 
	*/
	public function setKeyboardHandler(keybinding:String):Void;
	

	/**
	Sets the orientation.
	@arg oriantation 
	*/
	public function setOrientation(oriantation:Float):Void;
	

	/**
	Sets a new [[EditSession `EditSession`]] for the indicated editor.
	@arg session The new edit session
	@arg idx The editor's index you're interested in
	*/
	public function setSession(session:EditSession, idx:Float):Void;
	

	/**
	
	@arg splits The new number of splits
	*/
	public function setSplits(splits:Float):Void;
	

	/**
	Sets a theme for each of the available editors.
	@arg theme The name of the theme to set
	*/
	public function setTheme(theme:String):Void;
	

	/**
	
	*/
	public function UndoManagerProxy():Void;
	


}
