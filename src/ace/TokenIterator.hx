package ace;

@extern('TokenIterator')
extern class TokenIterator
{
	public function new(editSession:EditSession, initialRow:Int, initialColumn:Int):Void;
	/**
	Returns the current tokenized string.
	*/
	public function getCurrentToken():Dynamic;
	

	/**
	Returns the current column.
	*/
	public function getCurrentTokenColumn():Int;
	

	/**
	Returns the current row.
	*/
	public function getCurrentTokenRow():Int;
	

	/**
	Tokenizes all the items from the current point to the row prior in the document.
	*/
	public function stepBackward():Dynamic;
	

	/**
	Tokenizes all the items from the current point until the next row in the document. If the current point is at the end of the file, this function returns `null`. Otherwise, it returns the tokenized string.
	*/
	public function stepForward():Dynamic;
	


}
