package ace;

@extern('BackgroundTokenizer')
extern class BackgroundTokenizer
{
	/**
	Emits the `'update'` event. `firstRow` and `lastRow` are used to define the boundaries of the region to be updated.
	@arg firstRow The starting row region
	@arg lastRow The final row region
	*/
	public function fireUpdateEvent(firstRow:Float, lastRow:Float):Void;
	

	/**
	[Returns the state of tokenization at the end of a row.]{: #BackgroundTokenizer.getState}
	@arg row The row to get state at
	*/
	public function getState(row:Float):String;
	

	/**
	Gives list of tokens of the row. (tokens are cached)
	@arg row The row to get tokens at
	*/
	public function getTokens(row:Float):Dynamic;
	

	/**
	Sets a new document to associate with this object.
	@arg doc The new document to associate with
	*/
	public function setDocument(doc:Document):Void;
	

	/**
	Sets a new tokenizer for this object.
	@arg tokenizer The new tokenizer to use
	*/
	public function setTokenizer(tokenizer:Tokenizer):Void;
	

	/**
	Starts tokenizing at the row indicated.
	@arg startRow The row to start at
	*/
	public function start(startRow:Float):Void;
	

	/**
	Stops tokenizing.
	*/
	public function stop():Void;
	


}
