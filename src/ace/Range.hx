package ace;

@extern('Range')
extern class Range
{
	//<!--user-defined-->
	public var end:Dynamic;
	public var start:Dynamic;
	//<!--eof-user-defined-->

	/**
	Returns the part of the current `Range` that occurs within the boundaries of `firstRow` and `lastRow` as a new `Range` object.
	@arg firstRow The starting row
	@arg lastRow The ending row
	*/
	public function clipRows(firstRow:Float, lastRow:Float):Range;
	

	/**
	Returns a duplicate of the calling range.
	*/
	public function clone():Range;
	

	/**
	Returns a range containing the starting and ending rows of the original range, but with a column value of `0`.
	*/
	public function collapseRows():Range;
	

	/**
	Checks the row and column points with the row and column points of the calling range.
	@arg row A row point to compare with
	@arg column A column point to compare with
	*/
	public function compare(row:Float, column:Float):Float;
	

	/**
	Checks the row and column points with the row and column points of the calling range.
	@arg row A row point to compare with
	@arg column A column point to compare with
	*/
	public function compareEnd(row:Float, column:Float):Float;
	

	/**
	Checks the row and column points with the row and column points of the calling range.
	@arg row A row point to compare with
	@arg column A column point to compare with
	*/
	public function compareInside(row:Float, column:Float):Float;
	

	/**
	Checks the row and column points of `p` with the row and column points of the calling range.
	@arg p A point to compare with
	*/
	public function comparePoint(p:Range):Float;
	

	/**
	Compares `this` range (A) with another range (B).
	@arg range A range to compare with
	*/
	public function compareRange(range:Range):Float;
	

	/**
	Checks the row and column points with the row and column points of the calling range.
	@arg row A row point to compare with
	@arg column A column point to compare with
	*/
	public function compareStart(row:Float, column:Float):Float;
	

	/**
	Returns `true` if the `row` and `column` provided are within the given range. This can better be expressed as returning `true` if:

   this.start.row <= row <= this.end.row &&
   this.start.column <= column <= this.end.column
	@arg row A row to check for
	@arg column A column to check for
	*/
	public function contains(row:Float, column:Float):Bool;
	

	/**
	Checks the start and end points of `range` and compares them to the calling range. Returns `true` if the `range` is contained within the caller's range.
	@arg range A range to compare with
	*/
	public function containsRange(range:Range):Bool;
	

	/**
	Changes the row and column points for the calling range for both the starting and ending points. This method returns that range with a new row.
	@arg row A new row to extend to
	@arg column A new column to extend to
	*/
	public function extend(row:Float, column:Float):Range;
	

	/**
	Creates and returns a new `Range` based on the row and column of the given parameters.
	@arg start A starting point to use
	@arg end An ending point to use
	*/
	public function fromPoints(start:Range, end:Range):Range;
	

	/**
	Returns `true` if the `row` and `column` are within the given range.
	@arg row A row point to compare with
	@arg column A column point to compare with
	*/
	public function inside(row:Float, column:Float):Bool;
	

	/**
	Returns `true` if the `row` and `column` are within the given range's ending points.
	@arg row A row point to compare with
	@arg column A column point to compare with
	*/
	public function insideEnd(row:Float, column:Float):Bool;
	

	/**
	Returns `true` if the `row` and `column` are within the given range's starting points.
	@arg row A row point to compare with
	@arg column A column point to compare with
	*/
	public function insideStart(row:Float, column:Float):Bool;
	

	/**
	Returns `true` if passed in `range` intersects with the one calling this method.
	@arg range A range to compare with
	*/
	public function intersects(range:Range):Bool;
	

	/**
	Returns `true` if the caller's ending row point is the same as `row`, and if the caller's ending column is the same as `column`.
	@arg row A row point to compare with
	@arg column A column point to compare with
	*/
	public function isEnd(row:Float, column:Float):Bool;
	

	/**
	Returns `true` if and only if the starting row and column, and ending tow and column, are equivalent to those given by `range`.
	@arg range A range to check against
	*/
	public function isEqual(range:Range):Bool;
	

	/**
	Returns true if the range spans across multiple lines.
	*/
	public function isMultiLine():Bool;
	

	/**
	Returns `true` if the caller's starting row point is the same as `row`, and if the caller's starting column is the same as `column`.
	@arg row A row point to compare with
	@arg column A column point to compare with
	*/
	public function isStart(row:Float, column:Float):Bool;
	

	/**
	Sets the starting row and column for the range.
	@arg row A row point to set
	@arg column A column point to set
	*/
	public function setEnd(row:Float, column:Float):Void;
	

	/**
	Sets the starting row and column for the range.
	@arg row A row point to set
	@arg column A column point to set
	*/
	public function setStart(row:Float, column:Float):Void;
	

	/**
	Given the current `Range`, this function converts those starting and ending points into screen positions, and then returns a new `Range` object.
	@arg session The `EditSession` to retrieve coordinates from
	*/
	public function toScreenRange(session:EditSession):Range;
	

	/**
	Returns a string containing the range's row and column information, given like this:

   [start.row/start.column] -> [end.row/end.column]
	*/
	public function toString():String;
	


}
