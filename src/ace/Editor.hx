package ace;

@extern('Editor')
extern class Editor
{
	//<!--user-defined-->
	public var commands:Dynamic;
	public var renderer:VirtualRenderer;
	public var onTextInput:String->Void;
	//<!--eof-user-defined-->

	/**
	Returns an object indicating the currently selected rows. The object looks like this:

	{ first: range.start.row, last: range.end.row }
	*/
	@extern('$getSelectedRows') public function getSelectedRows():Dynamic;
	

	/**
	Returns the number of currently visibile rows.
	*/
	@extern('$getVisibleRowCount') public function getVisibleRowCount():Float;
	

	/**
	
	*/
	@extern('$highlightBrackets') public function highlightBrackets():Void;
	

	/**
	Executes a specific function, which can be anything that manipulates selected lines, such as copying them, duplicating them, or shifting them.
	@arg mover A method to call on each selected row
	*/
	@extern('$moveLines') public function moveLines(mover:Dynamic):Void;
	

	/**
	
	*/
	@extern('$updateHighlightActiveLine') public function updateHighlightActiveLine():Void;
	

	/**
	Adds the selection and cursor.
	@arg orientedRange A range containing a cursor
	*/
	public function addSelectionMarker(orientedRange:Range):Range;
	

	/**
	Outdents the current line.
	*/
	public function blockOutdent():Void;
	

	/**
	Blurs the current `textInput`.
	*/
	public function blur():Void;
	

	/**
	Attempts to center the current selection on the screen.
	*/
	public function centerSelection():Void;
	

	/**
	{:Selection.clearSelection}
	*/
	public function clearSelection():Void;
	

	/**
	Copies all the selected lines down one row.
	*/
	public function copyLinesDown():Float;
	

	/**
	Copies all the selected lines up one row.
	*/
	public function copyLinesUp():Float;
	

	/**
	Cleans up the entire editor.
	*/
	public function destroy():Void;
	

	/**
	Removes all the selections except the last added one.
	*/
	public function exitMultiSelectMode():Void;
	

	/**
	Attempts to find `needle` within the document. For more information on `options`, see [[Search `Search`]].
	@arg needle The text to search for (optional)
	@arg options An object defining various search properties
	@arg animate If `true` animate scrolling
	*/
	public function find(needle:String, options:Dynamic, animate:Bool):Void;
	

	/**
	Finds and selects all the occurences of `needle`.
	@arg needle The text to find
	@arg options The search options
	@arg additive keeps
	*/
	public function findAll(needle:String, options:Dynamic, additive:Bool):Float;
	

	/**
	Performs another search for `needle` in the document. For more information on `options`, see [[Search `Search`]].
	@arg options search options
	@arg animate If `true` animate scrolling
	*/
	public function findNext(options:Dynamic, animate:Bool):Void;
	

	/**
	Performs a search for `needle` backwards. For more information on `options`, see [[Search `Search`]].
	@arg options search options
	@arg animate If `true` animate scrolling
	*/
	public function findPrevious(options:Dynamic, animate:Bool):Void;
	

	/**
	Brings the current `textInput` into focus.
	*/
	public function focus():Void;
	

	/**
	Executes a command for each selection range.
	@arg cmd The command to execute
	@arg args Any arguments for the command
	*/
	public function forEachSelection(cmd:String, args:String):Void;
	

	/**
	Returns `true` if the behaviors are currently enabled. {:BehaviorsDef}
	*/
	public function getBehavioursEnabled():Bool;
	

	/**
	Returns the string of text currently highlighted.
	*/
	public function getCopyText():String;
	

	/**
	Gets the current position of the cursor.
	*/
	public function getCursorPosition():Dynamic;
	

	/**
	Returns the screen position of the cursor.
	*/
	public function getCursorPositionScreen():Dynamic;
	

	/**
	Returns the current mouse drag delay.
	*/
	public function getDragDelay():Float;
	

	/**
	{:VirtualRenderer.getFirstVisibleRow}
	*/
	public function getFirstVisibleRow():Float;
	

	/**
	Returns `true` if current lines are always highlighted.
	*/
	public function getHighlightActiveLine():Bool;
	

	/**
	Returns `true` if currently highlighted words are to be highlighted.
	*/
	public function getHighlightSelectedWord():Bool;
	

	/**
	Returns the keyboard handler.
	*/
	public function getKeyboardHandler():String;
	

	/**
	{:Search.getOptions} For more information on `options`, see [[Search `Search`]].
	*/
	public function getLastSearchOptions():Dynamic;
	

	/**
	{:VirtualRenderer.getLastVisibleRow}
	*/
	public function getLastVisibleRow():Float;
	

	/**
	Returns `true` if overwrites are enabled; `false` otherwise.
	*/
	public function getOverwrite():Bool;
	

	/**
	Returns the column number of where the print margin is.
	*/
	public function getPrintMarginColumn():Float;
	

	/**
	Returns `true` if the editor is set to read-only mode.
	*/
	public function getReadOnly():Bool;
	

	/**
	Returns the value indicating how fast the mouse scroll speed is.
	*/
	public function getScrollSpeed():Float;
	

	/**
	Returns the currently highlighted selection.
	*/
	public function getSelection():String;
	

	/**
	{:Selection.getRange}
	*/
	public function getSelectionRange():Range;
	

	/**
	Returns the current selection style.
	*/
	public function getSelectionStyle():String;
	

	/**
	Returns the current session being used.
	*/
	public function getSession():EditSession;
	

	/**
	Returns `true` if the fold widgets are shown.
	*/
	public function getShowFoldWidgets():Bool;
	

	/**
	Returns `true` if invisible characters are being shown.
	*/
	public function getShowInvisibles():Bool;
	

	/**
	Returns `true` if the print margin is being shown.
	*/
	public function getShowPrintMargin():Bool;
	

	/**
	{:VirtualRenderer.getTheme}
	*/
	public function getTheme():String;
	

	/**
	Returns the current session's content.
	*/
	public function getValue():String;
	

	/**
	Moves the cursor to the specified line number, and also into the indiciated column.
	@arg lineNumber The line number to go to
	@arg column A column number to go to
	@arg animate If `true` animates scolling
	*/
	public function gotoLine(lineNumber:Float, column:Float, animate:Bool):Void;
	

	/**
	Shifts the document to wherever "page down" is, as well as moving the cursor position.
	*/
	public function gotoPageDown():Void;
	

	/**
	Shifts the document to wherever "page up" is, as well as moving the cursor position.
	*/
	public function gotoPageUp():Void;
	

	/**
	Indents the current line.
	*/
	public function indent():Void;
	

	/**
	Inserts `text` into wherever the cursor is pointing.
	@arg text The new text to add
	*/
	public function insert(text:String):Void;
	

	/**
	Returns true if the current `textInput` is in focus.
	*/
	public function isFocused():Bool;
	

	/**
	Indicates if the entire row is currently visible on the screen.
	@arg row The row to check
	*/
	public function isRowFullyVisible(row:Float):Bool;
	

	/**
	Indicates if the row is currently visible on the screen.
	@arg row The row to check
	*/
	public function isRowVisible(row:Float):Bool;
	

	/**
	Moves the cursor's row and column to the next matching bracket.
	*/
	public function jumpToMatching():Void;
	

	/**
	Moves the cursor to the specified row and column. Note that this does not de-select the current selection.
	@arg row The new row number
	@arg column The new column number
	*/
	public function moveCursorTo(row:Float, column:Float):Void;
	

	/**
	Moves the cursor to the position indicated by `pos.row` and `pos.column`.
	@arg pos An object with two properties, row and column
	*/
	public function moveCursorToPosition(pos:Dynamic):Void;
	

	/**
	Shifts all the selected lines down one row.
	*/
	public function moveLinesDown():Float;
	

	/**
	Shifts all the selected lines up one row.
	*/
	public function moveLinesUp():Float;
	

	/**
	Moves a range of text from the given range to the given position. `toPosition` is an object that looks like this:

   { row: newRowLocation, column: newColumnLocation }
	@arg fromRange The range of text you want moved within the document
	@arg toPosition The location (row and column) where you want to move the text to
	*/
	public function moveText(fromRange:Range, toPosition:Dynamic):Range;
	

	/**
	Moves the cursor down in the document the specified number of times. Note that this does de-select the current selection.
	@arg times The number of times to change navigation
	*/
	public function navigateDown(times:Float):Void;
	

	/**
	Moves the cursor to the end of the current file. Note that this does de-select the current selection.
	*/
	public function navigateFileEnd():Void;
	

	/**
	Moves the cursor to the start of the current file. Note that this does de-select the current selection.
	*/
	public function navigateFileStart():Void;
	

	/**
	Moves the cursor left in the document the specified number of times. Note that this does de-select the current selection.
	@arg times The number of times to change navigation
	*/
	public function navigateLeft(times:Float):Void;
	

	/**
	Moves the cursor to the end of the current line. Note that this does de-select the current selection.
	*/
	public function navigateLineEnd():Void;
	

	/**
	Moves the cursor to the start of the current line. Note that this does de-select the current selection.
	*/
	public function navigateLineStart():Void;
	

	/**
	Moves the cursor right in the document the specified number of times. Note that this does de-select the current selection.
	@arg times The number of times to change navigation
	*/
	public function navigateRight(times:Float):Void;
	

	/**
	Moves the cursor to the specified row and column. Note that this does de-select the current selection.
	@arg row The new row number
	@arg column The new column number
	*/
	public function navigateTo(row:Float, column:Float):Void;
	

	/**
	Moves the cursor up in the document the specified number of times. Note that this does de-select the current selection.
	@arg times The number of times to change navigation
	*/
	public function navigateUp(times:Float):Void;
	

	/**
	Moves the cursor to the word immediately to the left of the current position. Note that this does de-select the current selection.
	*/
	public function navigateWordLeft():Void;
	

	/**
	Moves the cursor to the word immediately to the right of the current position. Note that this does de-select the current selection.
	*/
	public function navigateWordRight():Void;
	

	/**
	Called whenever a text "copy" happens.
	*/
	public function onCopy():Void;
	

	/**
	called whenever a text "cut" happens.
	*/
	public function onCut():Void;
	

	/**
	called whenever a text "paste" happens.
	*/
	public function onPaste():Void;
	

	/**
	{:UndoManager.redo}
	*/
	public function redo():Void;
	

	/**
	Removes words of text from the editor. A "word" is defined as a string of characters bookended by whitespace.
	@arg dir The direction of the deletion to occur, either "left" or "right"
	*/
	public function remove(dir:String):Void;
	

	/**
	Removes all the lines in the current selection
	*/
	public function removeLines():Void;
	

	/**
	Removes the selection marker.
	@arg range The selection range added with [[Editor.addSelectionMarker `addSelectionMarker()`]].
	*/
	public function removeSelectionMarker(range:Range):Void;
	

	/**
	Removes all the words to the right of the current selection, until the end of the line.
	*/
	public function removeToLineEnd():Void;
	

	/**
	Removes all the words to the left of the current selection, until the start of the line.
	*/
	public function removeToLineStart():Void;
	

	/**
	Removes the word directly to the left of the current selection.
	*/
	public function removeWordLeft():Void;
	

	/**
	Removes the word directly to the right of the current selection.
	*/
	public function removeWordRight():Void;
	

	/**
	Replaces the first occurance of `options.needle` with the value in `replacement`.
	@arg replacement The text to replace with
	@arg options The [[Search `Search`]] options to use
	*/
	public function replace(replacement:String, options:Dynamic):Void;
	

	/**
	Replaces all occurances of `options.needle` with the value in `replacement`.
	@arg replacement The text to replace with
	@arg options The [[Search `Search`]] options to use
	*/
	public function replaceAll(replacement:String, options:Dynamic):Void;
	

	/**
	{:VirtualRenderer.onResize}
	*/
	public function resize():Void;
	

	/**
	Scrolls the document to wherever "page down" is, without changing the cursor position.
	*/
	public function scrollPageDown():Void;
	

	/**
	Scrolls the document to wherever "page up" is, without changing the cursor position.
	*/
	public function scrollPageUp():Void;
	

	/**
	TODO scrolls a to line, if center == true, puts line in middle of screen or attempts to)
	@arg line The line to scroll to
	@arg center If `true`
	@arg animate If `true` animates scrolling
	@arg callback Function to be called when the animation has finished
	*/
	public function scrollToLine(line:Float, center:Bool, animate:Bool, _callback:Dynamic):Void;
	

	/**
	Moves the editor to the specified row.
	@arg row The row to move to
	*/
	public function scrollToRow(row:Float):Void;
	

	/**
	Selects all the text in editor.
	*/
	public function selectAll():Void;
	

	/**
	Finds the next occurence of text in an active selection and adds it to the selections.
	@arg dir The direction of lines to select: -1 for up, 1 for down
	@arg skip If `true`, removes the active selection range
	*/
	public function selectMore(dir:Float, skip:Bool):Void;
	

	/**
	Adds a cursor above or below the active cursor.
	@arg dir The direction of lines to select: -1 for up, 1 for down
	@arg skip If `true`, removes the active selection range
	*/
	public function selectMoreLines(dir:Float, skip:Bool):Void;
	

	/**
	Selects the text from the current position of the document until where a "page down" finishes.
	*/
	public function selectPageDown():Void;
	

	/**
	Selects the text from the current position of the document until where a "page up" finishes.
	*/
	public function selectPageUp():Void;
	

	/**
	Specifies whether to use behaviors or not. ["Behaviors" in this case is the auto-pairing of special characters, like quotation marks, parenthesis, or brackets.]{: #BehaviorsDef}
	@arg enabled Enables or disables behaviors
	*/
	public function setBehavioursEnabled(enabled:Bool):Void;
	

	/**
	Sets the delay (in milliseconds) of the mouse drag.
	@arg dragDelay A value indicating the new delay
	*/
	public function setDragDelay(dragDelay:Float):Void;
	

	/**
	Set a new font size (in pixels) for the editor text.
	@arg size A font size
	*/
	public function setFontSize(size:Float):Void;
	

	/**
	Determines whether or not the current line should be highlighted.
	@arg shouldHighlight Set to `true` to highlight the current line
	*/
	public function setHighlightActiveLine(shouldHighlight:Bool):Void;
	

	/**
	Determines if the currently selected word should be highlighted.
	@arg shouldHighlight Set to `true` to highlight the currently selected word
	*/
	public function setHighlightSelectedWord(shouldHighlight:Bool):Void;
	

	/**
	Sets a new keyboard handler.
	*/
	public function setKeyboardHandler(keyboardHandler:Dynamic):Void;
	

	/**
	Pass in `true` to enable overwrites in your session, or `false` to disable. If overwrites is enabled, any text you enter will type over any text after it. If the value of `overwrite` changes, this function also emites the `changeOverwrite` event.
	@arg overwrite Defines wheter or not to set overwrites
	*/
	public function setOverwrite(overwrite:Bool):Void;
	

	/**
	Sets the column defining where the print margin should be.
	@arg showPrintMargin Specifies the new print margin
	*/
	public function setPrintMarginColumn(showPrintMargin:Float):Void;
	

	/**
	If `readOnly` is true, then the editor is set to read-only mode, and none of the content can change.
	@arg readOnly Specifies whether the editor can be modified or not
	*/
	public function setReadOnly(readOnly:Bool):Void;
	

	/**
	Sets how fast the mouse scrolling should do.
	@arg speed A value indicating the new speed
	*/
	public function setScrollSpeed(speed:Float):Void;
	

	/**
	Indicates how selections should occur. By default, selections are set to "line". This function also emits the `'changeSelectionStyle'` event.
	@arg style The new selection style
	*/
	public function setSelectionStyle(style:String):Void;
	

	/**
	Sets a new editsession to use. This method also emits the `'changeSession'` event.
	@arg session The new session to use
	*/
	public function setSession(session:EditSession):Void;
	

	/**
	Indicates whether the fold widgets are shown or not.
	@arg show Specifies whether the fold widgets are shown
	*/
	public function setShowFoldWidgets(show:Bool):Void;
	

	/**
	If `showInvisibiles` is set to `true`, invisible characters&mdash;like spaces or new lines&mdash;are show in the editor.
	@arg showInvisibles Specifies whether or not to show invisible characters
	*/
	public function setShowInvisibles(showInvisibles:Bool):Void;
	

	/**
	If `showPrintMargin` is set to `true`, the print margin is shown in the editor.
	@arg showPrintMargin Specifies whether or not to show the print margin
	*/
	public function setShowPrintMargin(showPrintMargin:Bool):Void;
	

	/**
	{:VirtualRenderer.setStyle}
	*/
	public function setStyle(style:Dynamic):Void;
	

	/**
	{:VirtualRenderer.setTheme}
	*/
	public function setTheme(theme:Dynamic):Void;
	

	/**
	Sets the current document to `val`.
	@arg val The new value to set for the document
	@arg cursorPos Where to set the new value. `undefined` or 0 is selectAll, -1 is at the document start, and 1 is at the end
	*/
	public function setValue(val:String, cursorPos:Float):String;
	

	/**
	Splits the line at the current selection (by inserting an `'\n'`).
	*/
	public function splitLine():Void;
	

	/**
	Given the currently selected range, this function either comments all lines or uncomments all lines (depending on whether it's commented or not).
	*/
	public function toggleCommentLines():Void;
	

	/**
	Sets the value of overwrite to the opposite of whatever it currently is.
	*/
	public function toggleOverwrite():Void;
	

	/**
	Converts the current selection entirely into lowercase.
	*/
	public function toLowerCase():Void;
	

	/**
	Converts the current selection entirely into uppercase.
	*/
	public function toUpperCase():Void;
	

	/**
	Transposes current line.
	*/
	public function transposeLetters():Void;
	

	/**
	Transposes the selected ranges.
	@arg dir The direction to rotate selections
	*/
	public function transposeSelections(dir:Float):Void;
	

	/**
	{:UndoManager.undo}
	*/
	public function undo():Void;
	

	/**
	{:VirtualRenderer.unsetStyle}
	*/
	public function unsetStyle(style:Dynamic):Void;
	

	/**
	Updates the cursor and marker layers.
	*/
	public function updateSelectionMarkers():Void;
	


}
