package ace;

@extern('Tokenizer')
extern class Tokenizer
{
	/**
	Returns an object containing two properties: `tokens`, which contains all the tokens; and `state`, the current state.
	*/
	public function getLineTokens():Dynamic;
	


}
