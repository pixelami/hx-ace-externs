package ace;

@extern('Document')
extern class Document
{
	/**
	
	*/
	@extern('$clipPosition') public function clipPosition(position:Dynamic):Float;
	

	/**
	
	*/
	@extern('$detectNewLine') public function detectNewLine(text:Dynamic):Void;
	

	/**
	Splits a string of text on any newline (`\n`) or carriage-return ('\r') characters.
	@arg text The text to work with
	*/
	@extern('$split') public function split(text:String):String;
	

	/**
	Applies all the changes previously accumulated. These can be either `'includeText'`, `'insertLines'`, `'removeText'`, and `'removeLines'`.
	*/
	public function applyDeltas(deltas:Dynamic):Void;
	

	/**
	Creates a new `Anchor` to define a floating point in the document.
	@arg row The row number to use
	@arg column The column number to use
	*/
	public function createAnchor(row:Float, column:Float):Anchor;
	

	/**
	Returns all lines in the document as string array. Warning: The caller should not modify this array!
	*/
	public function getAllLines():String;
	

	/**
	Returns the number of rows in the document.
	*/
	public function getLength():Float;
	

	/**
	Returns a verbatim copy of the given line as it is in the document
	@arg row The row index to retrieve
	*/
	public function getLine(row:Float):String;
	

	/**
	Returns an array of strings of the rows between `firstRow` and `lastRow`. This function is inclusive of `lastRow`.
	@arg firstRow The first row index to retrieve
	@arg lastRow The final row index to retrieve
	*/
	public function getLines(firstRow:Float, lastRow:Float):String;
	

	/**
	Returns the newline character that's being used, depending on the value of `newLineMode`.
	*/
	public function getNewLineCharacter():String;
	

	/**
	[Returns the type of newlines being used; either `windows`, `unix`, or `auto`]{: #Document.getNewLineMode}
	*/
	public function getNewLineMode():String;
	

	/**
	[Given a range within the document, this function returns all the text within that range as a single string.]{: #Document.getTextRange.desc}
	@arg range The range to work with
	*/
	public function getTextRange(range:Range):String;
	

	/**
	Returns all the lines in the document as a single string, split by the new line character.
	*/
	public function getValue():String;
	

	/**
	
	@arg position The position to start inserting at
	@arg text A chunk of text to insert
	*/
	public function insert(position:Float, text:String):Float;
	

	/**
	Inserts `text` into the `position` at the current row. This method also triggers the `'change'` event.
	@arg position The position to insert at
	@arg text A chunk of text
	*/
	public function insertInLine(position:Float, text:String):Dynamic;
	

	/**
	Inserts the elements in `lines` into the document, starting at the row index given by `row`. This method also triggers the `'change'` event.
	@arg row The index of the row to insert at
	@arg lines An array of strings
	*/
	public function insertLines(row:Float, lines:Array<Dynamic>):Dynamic;
	

	/**
	Inserts a new line into the document at the current row's `position`. This method also triggers the `'change'` event.
	@arg position The position to insert at
	*/
	public function insertNewLine(position:String):Dynamic;
	

	/**
	Returns `true` if `text` is a newline character (either `\r\n`, `\r`, or `\n`).
	@arg text The text to check
	*/
	public function isNewLine(text:String):Bool;
	

	/**
	Removes the `range` from the document.
	@arg range A specified Range to remove
	*/
	public function remove(range:Range):Dynamic;
	

	/**
	Removes the specified columns from the `row`. This method also triggers the `'change'` event.
	@arg row The row to remove from
	@arg startColumn The column to start removing at
	@arg endColumn The column to stop removing at
	*/
	public function removeInLine(row:Float, startColumn:Float, endColumn:Float):Dynamic;
	

	/**
	Removes a range of full lines. This method also triggers the `'change'` event.
	@arg firstRow The first row to be removed
	@arg lastRow The last row to be removed
	*/
	public function removeLines(firstRow:Float, lastRow:Float):String;
	

	/**
	Removes the new line between `row` and the row immediately following it. This method also triggers the `'change'` event.
	@arg row The row to check
	*/
	public function removeNewLine(row:Float):Void;
	

	/**
	Replaces a range in the document with the new `text`.
	@arg range A specified Range to replace
	@arg text The new text to use as a replacement
	*/
	public function replace(range:Range, text:String):Dynamic;
	

	/**
	Reverts any changes previously applied. These can be either `'includeText'`, `'insertLines'`, `'removeText'`, and `'removeLines'`.
	*/
	public function revertDeltas(deltas:Dynamic):Void;
	

	/**
	[Sets the new line mode.]{: #Document.setNewLineMode.desc}
	@arg newLineMode [The newline mode to use; can be either `windows`, `unix`, or `auto`]{: #Document.setNewLineMode.param}
	*/
	public function setNewLineMode(newLineMode:String):Void;
	

	/**
	Replaces all the lines in the current `Document` with the value of `text`.
	@arg text The text to use
	*/
	public function setValue(text:String):Void;
	


}
