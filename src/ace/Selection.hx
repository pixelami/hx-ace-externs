package ace;

@extern('Selection')
extern class Selection
{
	/**
	Adds a range to a selection by entering multiselect mode, if necessary.
	@arg range The new range to add
	@arg $blockChangeEvents Whether or not to block changing events
	*/
	public function addRange(range:Range, blockChangeEvents:Bool):Void;
	

	/**
	[Empties the selection (by de-selecting it). This function also emits the `'changeSelection'` event.]{: #Selection.clearSelection}
	*/
	public function clearSelection():Void;
	

	/**
	Gets the current position of the cursor.
	*/
	public function getCursor():Float;
	

	/**
	[Returns the [[Range]] for the selected text.]{: #Selection.getRange}
	*/
	public function getRange():Range;
	

	/**
	Returns an object containing the `row` and `column` of the calling selection anchor.
	*/
	public function getSelectionAnchor():Dynamic;
	

	/**
	Returns an object containing the `row` and `column` of the calling selection lead.
	*/
	public function getSelectionLead():Dynamic;
	

	/**
	Returns `true` if the selection is going backwards in the document.
	*/
	public function isBackwards():Bool;
	

	/**
	Returns `true` if the selection is empty.
	*/
	public function isEmpty():Bool;
	

	/**
	Returns `true` if the selection is a multi-line.
	*/
	public function isMultiLine():Bool;
	

	/**
	Merges overlapping ranges ensuring consistency after changes
	*/
	public function mergeOverlappingRanges():Void;
	

	/**
	Moves the cursor to position indicated by the parameters. Negative numbers move the cursor backwards in the document.
	@arg rows The number of rows to move by
	@arg chars The number of characters to move by
	*/
	public function moveCursorBy(rows:Float, chars:Float):Void;
	

	/**
	Moves the cursor down one row.
	*/
	public function moveCursorDown():Void;
	

	/**
	Moves the cursor to the end of the file.
	*/
	public function moveCursorFileEnd():Void;
	

	/**
	Moves the cursor to the start of the file.
	*/
	public function moveCursorFileStart():Void;
	

	/**
	Moves the cursor left one column.
	*/
	public function moveCursorLeft():Void;
	

	/**
	Moves the cursor to the end of the line.
	*/
	public function moveCursorLineEnd():Void;
	

	/**
	Moves the cursor to the start of the line.
	*/
	public function moveCursorLineStart():Void;
	

	/**
	Moves the cursor to the word on the left.
	*/
	public function moveCursorLongWordLeft():Void;
	

	/**
	Moves the cursor to the word on the right.
	*/
	public function moveCursorLongWordRight():Void;
	

	/**
	Moves the cursor right one column.
	*/
	public function moveCursorRight():Void;
	

	/**
	Moves the cursor to the row and column provided. [If `preventUpdateDesiredColumn` is `true`, then the cursor stays in the same column position as its original point.]{: #preventUpdateBoolDesc}
	@arg row The row to move to
	@arg column The column to move to
	@arg keepDesiredColumn [If `true`, the cursor move does not respect the previous column]{: #preventUpdateBool}
	*/
	public function moveCursorTo(row:Float, column:Float, keepDesiredColumn:Bool):Void;
	

	/**
	Moves the selection to the position indicated by its `row` and `column`.
	@arg position The position to move to
	*/
	public function moveCursorToPosition(position:Dynamic):Void;
	

	/**
	Moves the cursor to the screen position indicated by row and column. {:preventUpdateBoolDesc}
	@arg row The row to move to
	@arg column The column to move to
	@arg keepDesiredColumn {:preventUpdateBool}
	*/
	public function moveCursorToScreen(row:Float, column:Float, keepDesiredColumn:Bool):Void;
	

	/**
	Moves the cursor up one row.
	*/
	public function moveCursorUp():Void;
	

	/**
	Gets list of ranges composing rectangular block on the screen
	@arg screenCursor The cursor to use
	@arg screenAnchor The anchor to use
	@arg includeEmptyLines If true, this includes ranges inside the block which are empty due to clipping
	*/
	public function rectangularRangeBlock(screenCursor:Cursor, screenAnchor:Anchor, includeEmptyLines:Bool):Range;
	

	/**
	Selects all the text in the document.
	*/
	public function selectAll():Void;
	

	/**
	Selects a word, including its right whitespace.
	*/
	public function selectAWord():Void;
	

	/**
	Moves the selection down one row.
	*/
	public function selectDown():Void;
	

	/**
	Moves the selection to the end of the file.
	*/
	public function selectFileEnd():Void;
	

	/**
	Moves the selection to the start of the file.
	*/
	public function selectFileStart():Void;
	

	/**
	Moves the selection left one column.
	*/
	public function selectLeft():Void;
	

	/**
	Selects the entire line.
	*/
	public function selectLine():Void;
	

	/**
	Moves the selection to the end of the current line.
	*/
	public function selectLineEnd():Void;
	

	/**
	Moves the selection to the beginning of the current line.
	*/
	public function selectLineStart():Void;
	

	/**
	Moves the selection right one column.
	*/
	public function selectRight():Void;
	

	/**
	Moves the selection cursor to the indicated row and column.
	@arg row The row to select to
	@arg column The column to select to
	*/
	public function selectTo(row:Float, column:Float):Void;
	

	/**
	Moves the selection cursor to the row and column indicated by `pos`.
	@arg pos An object containing the row and column
	*/
	public function selectToPosition(pos:Dynamic):Void;
	

	/**
	Moves the selection up one row.
	*/
	public function selectUp():Void;
	

	/**
	Moves the selection to highlight the entire word.
	*/
	public function selectWord():Void;
	

	/**
	Moves the selection to the first word on the left.
	*/
	public function selectWordLeft():Void;
	

	/**
	Moves the selection to the first word on the right.
	*/
	public function selectWordRight():Void;
	

	/**
	Sets the row and column position of the anchor. This function also emits the `'changeSelection'` event.
	@arg row The new row
	@arg column The new column
	*/
	public function setSelectionAnchor(row:Float, column:Float):Void;
	

	/**
	Sets the selection to the provided range.
	@arg range The range of text to select
	@arg reverse Indicates if the range should go backwards (`true`) or not
	*/
	public function setSelectionRange(range:Range, reverse:Bool):Void;
	

	/**
	Shifts the selection up (or down, if [[Selection.isBackwards `isBackwards()`]] is true) the given number of columns.
	@arg columns The number of columns to shift by
	*/
	public function shiftSelection(columns:Float):Void;
	

	/**
	Removes a Range containing pos (if it exists).
	@arg pos The position to remove, as a `{row, column}` object
	*/
	public function substractPoint(pos:Range):Range;
	


}
