package ace;

@extern('Search')
extern class Search
{
	/**
	
	@arg session The session to search with
	*/
	@extern('$matchIterator') public function matchIterator(session:EditSession):String;
	

	/**
	Searches for `options.needle`. If found, this method returns the [[Range `Range`]] where the text first occurs. If `options.backwards` is `true`, the search goes backwards in the session.
	@arg session The session to search with
	*/
	public function find(session:EditSession):Range;
	

	/**
	Searches for all occurances `options.needle`. If found, this method returns an array of [[Range `Range`s]] where the text first occurs. If `options.backwards` is `true`, the search goes backwards in the session.
	@arg session The session to search with
	*/
	public function findAll(session:EditSession):Range;
	

	/**
	[Returns an object containing all the search options.]{: #Search.getOptions}
	*/
	public function getOptions():Dynamic;
	

	/**
	Searches for `options.needle` in `input`, and, if found, replaces it with `replacement`.
	@arg input The text to search in
	@arg replacement The replacing text
	*/
	public function replace(input:String, replacement:String):String;
	

	/**
	Sets the search options via the `options` parameter.
	@arg options An object containing all the new search properties
	*/
	public function set(options:Dynamic):Search;
	


}
